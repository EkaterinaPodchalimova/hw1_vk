# Домашняя работа VK API

### Данный проект подготовлен в рамках курса Семинар настаника. 
### Стэк: Python, Jupyter Notebook, GitLab, Requests, Pandas
### Данные были взяты из открытого Api VK и подготовлены для дальнейшего использования в DataLens.

- Для авторизации запустите API.get_code() Вас перебросит на страничку с авторизацией
- Для получения токена API.get_token() Вам выдаст токен
- Если Вы хотите получить данные пользователей, вызовите метод API.user_get(list), где list - список id для пользователей
- Два теста положены в test_app.py
- Если Вы хотите посмотреть тесты запустите команды в терминале:
1) pip install -r requirements.txt
2) python -m pytest tests --junitxml=test_report.xml --cov=utils
- Ссылка на дашборд https://datalens.yandex/vfgrx53kj48sk

- Автор: Сталева Екатерина (АДД)