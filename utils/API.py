import requests
import os
from dotenv import load_dotenv
import pandas as pd

class API():
    def __init__(self):
        load_dotenv()
        self.CODE = os.getenv('CODE')
        self.CLIENT_ID = os.getenv('CLIENT_ID')
        self.ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
        self.CLIENT_SECRET = os.getenv('CLIENT_SECRET')
        self.city_id = 134

    def get_code(self):
        token_params = {
        'client_id': self.CLIENT_ID,
        'redirect_uri': 'https://oauth.vk.com/blank.html',
        'scope': 'friends,photos,wall,email,phone_number,docs'
        }
        auth_url = f'https://oauth.vk.com/authorize?client_id={token_params["client_id"]}&display=page&redirect_uri={token_params["redirect_uri"]}&response_type=code&scope={token_params["scope"]}&v=5.131'
        return f'{auth_url} Нажмите для аутентификации'
    
    def get_token(self):
        token_params = {
            'client_id': self.CLIENT_ID,
            'client_secret': self.CLIENT_SECRET,
            'redirect_uri': 'https://oauth.vk.com/blank.html',
            'code': self.CODE
            }
        token_url = 'https://oauth.vk.com/access_token'
        response = requests.post(token_url, data=token_params)
        df = response.json()
        if 'email' in df:
            self.ACCESS_TOKEN = df['access_token']
            return 'Токен получен'
        else:
            return 'Авторизуйтесь'
        
    def take_users(self, sex):
        search_params = {
            'access_token': self.ACCESS_TOKEN,
            'city': self.city_id,
            'count': 1000,
            'fields': 'first_name,bdate,sex',
            'sex': sex,
            'v': 5.199
            }
        search_url =  'https://api.vk.com/method/users.search'
        response = requests.get(search_url, params = search_params)
        users = response.json()
        users = users['response']['items']
        return pd.DataFrame(users)
    
    def user_get(self, user_ids):
        search_params = {
            'access_token': self.ACCESS_TOKEN,
            'user_ids': user_ids,
            'fields': 'city,bdate,sex,education',
            'v': 5.199
            }
        search_url =  'https://api.vk.com/method/users.get'
        response = requests.get(search_url, params = search_params)
        user = response.json()
        user = user['response']
        return  pd.DataFrame(user)
        
    